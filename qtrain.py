import spacy as scy
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.svm import LinearSVC
from sklearn.metrics import accuracy_score
from sklearn.externals import joblib

print ("Loading the language model...")

nlp = scy.load('en')

print ("Loading the data...")
data = pd.read_csv('LabelledData.csv',sep=',,,', names=['QUESTION','CLASS'], engine='python')

labels = pd.DataFrame(data.CLASS.str.strip(),columns=['CLASS'])
qcorpus = data.QUESTION.tolist()

print ("Extracting features...")
features = pd.DataFrame(columns=['first','second','first_pos','second_pos','last_pos','root_pos'])

for i,doc in enumerate(qcorpus):
    try:
        doc = nlp(u''+doc)
        first = doc[0].text
        second = doc[1].text
        first_pos = doc[0].tag_
        second_pos = doc[1].tag_ 
        
        last_pos = doc[-1].tag_        
        if doc[-1].pos_ == 'PUNCT':
            last_pos = doc[-2].tag_
            
        for token in doc:
            if token.dep_ == 'ROOT':
                root_pos = token.tag_
                
        entry = pd.DataFrame([[first,second,first_pos,second_pos,last_pos,root_pos]],columns=['first','second','first_pos','second_pos','last_pos','root_pos'])
        features = features.append(entry).reset_index(drop=True)
        
    except Exception as e:
        print (str(e))
        continue

#ONE HOT ENCODING the features
features_new = pd.get_dummies(features)

fcols = features_new.columns
f = open('fcols.txt', 'w')
for item in fcols.tolist():
  f.write("%s\n" % item)



X_train, X_test, y_train, y_test = train_test_split(features_new,labels, test_size=0.05)
#Converting the dataframe to nd array
X_atrain, X_atest, y_atrain, y_atest = X_train.values, X_test.values, y_train.values, y_test.values


print ('Training the model..')
#initialising the classifier
clf = LinearSVC()
#fitting the model
model = clf.fit(X_atrain,y_atrain.ravel())

model_file_name = 'qclassifier.pkl'
print ('Saving the model...')
joblib.dump(model, model_file_name) 
print ("Model saved as : {}".format(model_file_name))