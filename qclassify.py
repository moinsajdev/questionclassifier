from sklearn.externals import joblib
import spacy
import pandas as pd
import numpy as np
import sys

def create_test_features(doc):
    fcols = open('fcols.txt').read().strip().split('\n')
    test_feature = pd.DataFrame(np.zeros(len(fcols)).astype(int).reshape(1,-1),columns=fcols)
    doc = nlp(u''+doc)
    featdict = {
    'first' : doc[0].text,
    'second' : doc[1].text,
    'first_pos' : doc[0].tag_,
    'second_pos' : doc[1].tag_
    }
    for token in doc:
        if token.dep_ == 'ROOT':
            featdict['root_pos'] = token.tag_
    
    featdict['last_pos'] = doc[-1].tag_        
    if doc[-1].pos_ == 'PUNCT':
        featdict['last_pos'] = doc[-2].tag_
    
    for feat in featdict:
        feat_col = "_".join((feat,featdict[feat]))
        if feat_col in test_feature:
            test_feature.loc[0,feat_col] = 1
    
    return test_feature

def classify(doc,model):
    X = create_test_features(doc)
    y = model.predict(X)
    return y[0]

if __name__ == '__main__':

    #loading the language model
    nlp = spacy.load('en')

    #default doc
    doc = 'what is the job to be done?'

    if len(sys.argv) > 1:
        doc = " ".join(sys.argv[1:])
        

    model = joblib.load('qclassifier.pkl')

    res = classify(doc,model)
    print("The question : \"{}\" is classified as :\n\"{}\"".format(doc,res))