# QuestionClassifier

Given a question document, has to classify if the question context is 'what', 'when' , 'who', 'affirmative' and 'unknown' if not classifiable.

Libraries used:

	1. python 2.7
	2. Spacy with 'en' language model
	3. sklearn
	4. pandas
	5. numpy
	6. jupyter notebook
	

*git clone https://moinsajdev@bitbucket.org/moinsajdev/questionclassifier.git*

files:

**train.py**, to train the data and store the model as 'qclassifier.pkl'

**qclassify.py**, takes arguments as question document input and responds with its class

**qclassifier.ipynb**, notebook containing process of the project

ex:

>python qclassify.py is this okay ?

>The question : "is this okay ?" is classified as :

>"affirmation"
